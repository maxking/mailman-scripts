# This scripts resets all user passwords for GNU Mailman Core 3.0+
#
# Author: Abhilash Raj <maxking@asynchronous.in>
#
# Usage:
#
# This script should be run in the environment where mailmanclient is
# importable, i.e. `import mailmanclient` should work. If you have MailmanClient
# installed globally, just run this script using your global python3, if you
# have it installed in a virtual environment, use the virtual environment to do
# the same.

import sys
import secrets

from mailmanclient import Client


# This is the number of bytes in the new password, that was set for a user. The
# text is base64 encoded so it return approx 1.3x characters. The password is
# generated using secrets module in Python's standard library.
# https://docs.python.org/3/library/secrets.html#secrets.token_urlsafe
PASSWORD_SIZE = 16


def print_usage():
    print("Usage: \n"
          "python3 reset-user-passwords.py <mailman_api_path> <user> <passowrd> \n"             # noqa
          "\n"
          "Example: \n"
          "python3 reset-user-passwords.py http://localhost:8001/3.0 restadmin restpass \n")    # noqa
    exit(1)


def get_random_password():
    """Generate a random password for a user.
    """
    return secrets.token_urlsafe(PASSWORD_SIZE)


def reset_password(user):
    """Given a mailmanclient..User object, reset its password to None in
    the database.
    """
    user.password = get_random_password()
    user.save()
    print("Password for {} was reset.".format(user))


def get_all_users(client):
    """Given a mailmanclient.Client instance, returns an iterator of paginated
    user records.
    """
    page = client.get_user_page(count=50, page=1)
    while True:
        for user in page:
            yield user
        if page.has_next:
            page = page.next
        else:
            break


def main():
    if len(sys.argv) != 4:
        print_usage()
    print(sys.argv)
    _, url, user, password = sys.argv
    client = Client(url, user, password)
    for user in get_all_users(client):
        reset_password(user)


if __name__ == '__main__':
    main()
