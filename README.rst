Mailman 3 Scripts
=================

This repository consists of scripts for administration of GNU Mailman 3
installation. Some of these scripts use the REST API interface of the Core and
others directory interact with the Core's Internal API.

List of Scripts
---------------

* ``reset-user-passwords.py``: This script is used to reset the passwords of
  _all_ users to a random value. It uses pagination to fetch user records so it
  doesn't hog on your memory if you have millions of users. It may take some
  time to reset all passwords if you have a huge number of users.

  This uses Core's REST API to reset passwords and generates passwords using
  Python's ``secrets`` module, which was added in Python 3.6
